# Install Vagrant

# Fire up the VM
vagrant up

# Reprovision the vlab
vagrant up --provision

# Shutdown the vlab
vagrant halt

# Delete the vlab
vagrant destroy

# Shared Directories
/vlab-files to _files/share
/vlab-provision to _files/share/provision/

# Exposed Ports
VM: 80 / Host: 8082
