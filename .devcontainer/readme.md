## Deployment Sidecar
Using the technology of conatiners and virtual machines to setup the vlab environment

### Use vscode devcontainer
#### setup a ready to use vlab environment (local)
- install vscode (https://code.visualstudio.com/)
- install remote development extension (https://github.com/Microsoft/vscode-remote-release)
- start devcontainer

#### Default tools
- Ubuntu jammy
- Docker in Docker
- Ansible
- python
- Molecule (Docker)

#### Usecase install required python modules
- add names to requirements.txt

#### Usecase install required python modules for ansible
- sudo /usr/local/py-utils/venvs/ansible-core/bin/python3 -m pip install requests
- sudo /usr/local/py-utils/venvs/ansible-core/bin/python3 -m pip install docker

#### Usecase CI/CD Pipeline on devcontainer
export CI_API_V4_URL="https://gitlab.com/api/v4"
export CI_PROJECT_ID="3294373"
export CI_COMMIT_TAG="f4883d650a9e7cd04f0b0e308ab0a91bd388543a"