# SAM vLab Template
This is a Gitlab Repository to crate a Virtual Lab Environment.

## Instructions for your first Virtual Lab:

### Get more Information about this lab (Online):
  * Browse with Gitlab Pages https://rstumpner.gitlab.io/sam-vlab-template

### Get more Information about this lab (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-template
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

### Setup the Virtual Lab Environment with vscode (default)

Pre-Requirements:
  * 4 GB Memory (minimal)
  * 2 x CPU Cores
  * Installation of vscode (https://www.virtualbox.org/)
  * Install the Remote Development Extension
  * WSL
  * Docker

Run the vlab:
  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-devops-puppet
  * Start the Setup of this vLAB Environment with devconatiner
    ```md
    - crtl + shift + P
    - reopen in conainer
     ```

### Setup the Virtual Lab Environment with vagrant

Pre-Requirements:
  * 4 GB Memory (minimal)
  * 2 x CPU Cores
  * Installation of Virtualbox (https://www.virtualbox.org/)

The vLAB Environment:
  * puppetclient (Ubuntu 16.04 with Puppet Agent )
  * puppetserver (Ubuntu 16.04 with Puppet Server)
  * puppetbolt (Ubuntu 16.04 with Puppet bolt )

Automatic Setup with Vagrant (https://www.vagrantup.com/) (local):
  * Download and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.deb?_ga=2.257941326.439840422.1522128825-1814262215.1522128825
      * dpkg -i vagrant_2.0.3_x86_64.deb
    * On Windows
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.msi?_ga=2.89037278.439840422.1522128825-1814262215.1522128825
    * on macOS
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.dmg?_ga=2.257941326.439840422.1522128825-1814262215.1522128825

  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-devops-puppet
  * Start the Setup the vLAB Environment with Vagrant
    ```md
       cd sam-vlab-devops-puppet/vlab/vagrant/
       vagrant up
       ```
  * Check the vLAB Setup
     ```md
     vagrant status
     ```
  * Login to work with a Node
    ```md
    vagrant ssh puppetclient
    ```
#### Troubleshooting
